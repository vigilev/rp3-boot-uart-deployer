import serial
import argparse
import sys
from enum import Enum
import logging
import time


class Deployer:
    def __init__(self, srl: serial.Serial):
        self.serial = srl
    
    def connect(self):
        correct_opcode = "RP3boot"
        op_code = self.serial.read(len(correct_opcode)).decode("ascii")
        if op_code != correct_opcode:
            raise Exception(f"Operation code is wrong: {op_code} instead of {correct_opcode}")
        logging.info("connected")
    
    def get_bootloader_metadata(self) -> (int, int):
        start = int.from_bytes(self.serial.read(8), "little")
        end = int.from_bytes(self.serial.read(8), "little")
        logging.info(f"start: {hex(start)}")
        logging.info(f"end: {hex(end)}")

    def send_address(self, address):
        self.serial.write(address.to_bytes(8, "little"))
        ack_adr = int.from_bytes(self.serial.read(8), "little")
        if ack_adr != address:
            raise Exception(f"Wrong adr confirmed: {ack_adr} instead of {address}")
        logging.info(f"address confirmed: {hex(ack_adr)}")

    def send_size(self, size):
        self.serial.write(size.to_bytes(4, "little"))
        ack_size = int.from_bytes(self.serial.read(4), "little")
        if ack_size != size:
            raise Exception(f"Wrong size confirmed: {ack_size} instead of {size}")
        logging.info(f"size confirmed: {ack_size}")
    
    def send_kernel(self, kernel_data: bytes):

        logging.info("start sending kernel")
        #self.serial.write(kernel_data)
        for (i, b) in list(enumerate(kernel_data)):
            self.serial.write(bytes([b]))
            ack = int.from_bytes(self.serial.read(1), "little")
            if(b != ack):
                logging.debug(f"{i}: {bytes([b])} vs {bytes([ack])}")
        logging.info("kernel sent")

        checksum = 0
        for byte in kernel_data:
            checksum += byte
        checksum = checksum & 0xFFFFFFFF

        logging.debug("checksum calculating locally")
        received_checksum = checksum # int.from_bytes(self.serial.read(4), "little")
        if checksum != received_checksum:
            raise Exception(f"Wrong checksum received: {hex(received_checksum)} instead of {hex(checksum)}")
        logging.info(f"checksum ok ({hex(checksum)})")


    def deploy(self, kernel_data: bytes):
        self.connect()
        self.get_bootloader_metadata()
        self.send_address(0)
        self.send_size(len(kernel_data))
        self.send_kernel(kernel_data)
              

def main(argv):
    logging.basicConfig(encoding='utf-8', level=logging.DEBUG)
    parser = argparse.ArgumentParser(
        prog="UART deployer",
        description="UART deployer to Raspberry Pi 3 based on the respctive UART bootloader")
    parser.add_argument("--kernel", "-k", 
        dest="kernel",
        required=True,
        help="Path to kernel .img file that will be deployed")
    parser.add_argument("--device", "-d",
        dest="device",
        required=True,
        help="Pathe to file that reprsents uart device")
    parser.add_argument("--baudrate", "-b",
        dest="baudrate",
        default=115200,
        help="Baudrate for the device")
    args = parser.parse_args(argv[1:])



    if args.kernel and args.device and args.baudrate:
        logging.debug(f"args: {(args.kernel, args.device, args.baudrate)}")
        try:
            d = Deployer(serial.Serial(args.device, args.baudrate))
            kernel_data = bytes(0)
            with open(args.kernel, "rb") as file:
                kernel_data = file.read()
            if len(kernel_data) == 0:
                raise FileNotFoundError(f"File reading error")
            d.deploy(kernel_data)

            # print output after deploying
            time.sleep(2)
            print(d.serial.read_all())

        except Exception as e:
            logging.error(f"[{e.args}]", exc_info=True)



if __name__ == '__main__':
    main(sys.argv)

